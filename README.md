# README #

This template is [Rich Vuduc](http://vuduc.org)'s starter for proposal documents and technical reports. It derives from the [Tufte LaTeX package](https://tufte-latex.github.io/tufte-latex/), and was most recently tested against version 3.5.2 of that package.

To use this template with the `Makefile` provided, you'll need the [latexmk](http://www.ctan.org/pkg/latexmk/) utility.
