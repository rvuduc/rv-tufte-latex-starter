.DEFAULT_GOAL := all

LATEXMK = latexmk -f -shell-escape -pdf

.PHONY: all
all: main.pdf

.PHONY: main.pdf
main.pdf:
	$(LATEXMK) main

clean:
	rm -f *~ *.o core
	$(LATEXMK) -C main

# eof
